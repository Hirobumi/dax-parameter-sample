from sample_system.system import *
from dax.scan import DaxScan


class SampleRabi(DaxScan, SampleSystem, Experiment):
    def build_scan(self):
        self.add_scan('t', 'T', Scannable(RangeScan(89.9e6,90e6,101),unit="MHz"))
        self.setattr_argument("Frequency", NumberValue(90e6, unit="MHz"))
        self.setattr_argument("ChirpSpan",NumberValue(10e3,ndecimals=4,unit="kHz"))

    def host_setup(self):
        self.dax_init()
        
    def drg_spectroscopy_calculate(self):
        for t in self._dax_scan_elements:
            t = t[0].t
            self._parameter.spectroscopy_calculate(self.Frequency, self.ChirpSpan, t)

    @kernel
    def run_point(self, point, index):
        self.core.reset()
        self.core.break_realtime()
        self.single_atom_prepare() # Not implemented
        self.single_atom_detection() # Not implemented
        self.spectroscopy.raman_spectroscopy_drg(index.t)
        self.single_atom_detection() # Not implemented
        self.core.wait_until_mu(now_mu)