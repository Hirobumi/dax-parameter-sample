from sample_system.system import *
from dax.scan import DaxScan


class SampleSpectroscopy(DaxScan, SampleSystem, Experiment):
    def build_scan(self):
        self.add_scan('freq', 'Frequency', Scannable(RangeScan(89.9e6,90e6,101),unit="MHz"))
        self.setattr_argument("ChirpSpan",NumberValue(10e3,ndecimals=4,unit="kHz"))
        self.setattr_argument("ChirpT", NumberValue(100e-6, ndecimals=2,unit="us"))

    def host_setup(self):
        self.dax_init()
        self.drg_spectroscopy_calculate()
        
    def drg_spectroscopy_calculate(self):
        for point, index in self._dax_scan_elements:
            f = point.freq
            ftw = frequency_to_ftw(f)
            drg_start_ftw = frequency_to_ftw(f-self.ChirpSpan/2)
            drg_stop_ftw = frequency_to_ftw(f+self.ChirpSpan/2)

            point.ftw = ftw
            point.drg_start_ftw = drg_start_ftw
            point.drg_stop_ftw = drg_stop_ftw
            point.t = self.ChirpT

    @kernel
    def run_point(self, point, index):
        self.core.reset()
        self.core.break_realtime()
        self.single_atom_prepare() # Not implemented
        self.single_atom_detection() # Not implemented
        self.spectroscopy.raman_spectroscopy_drg(point)
        self.single_atom_detection() # Not implemented
        self.core.wait_until_mu(now_mu)