from dax.experiment import *
from sample_system.modules.laser import LaserModule
from sample_system.modules.parameter import ParameterModule


class SpectroscopyService(DaxService):
    def build(self):
        self._cw = self.registry.find_module(LaserModule)
        self._parameter = self.registry.find_module(ParameterModule)

    def init(self):
        ...

    def post_init(self):
        ...

    @kernel
    def raman_spectroscopy_drg(self, point):
        ftw = point.ftw
        drg_start_ftw = point.drg_start_ftw
        drg_stop_ftw = point.drg_stop_ftw
        t = point.t

        self._cw.raman_beam1.set_mu(ftw)
        
        with parallel:
            self._cw.raman_beam1.square_pulse(t)
            self._cw.raman_beam2.chirp_pulse(drg_start_ftw, drg_stop_ftw)