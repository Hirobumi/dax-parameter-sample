from dax.experiment import *

from sample_system.modules.laser import LaserModule
from sample_system.modules.parameter import ParameterModule
from sample_system.service.spectroscopy import SpectroscopyService


class SampleSystem(DaxSystem):
    def build(self):
        self._cw = LaserModule(self, 'laser')
        self._parameter = ParameterModule(self, 'parameter')
        self.spectroscopy = SpectroscopyService(self, 'spectroscopy')

    @kernel
    def init(self):
        ...