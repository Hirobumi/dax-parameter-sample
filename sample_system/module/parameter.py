from dax.experiment import *


class ParameterModule(DaxModule):
    def build(self):
        self.spectroscopy_drg_start_ftw = []
        self.spectroscopy_drg_stop_ftw = []
        self.spectroscopy_t = []

    def spectroscopy_calculate(self, f, f_span, t):
        drg_start_ftw = frequency_to_ftw(f-f_span/2)
        drg_stop_ftw = frequency_to_ftw(f+f_span/2)

        self.spectroscopy_drg_start_ftw.append(drg_start_ftw)
        self.spectroscopy_drg_stop_ftw.append(drg_stop_ftw)
        self.spectroscopy_t.append(t)