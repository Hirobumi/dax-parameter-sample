from dax.experiment import *


class DDSModule(DaxModule):
    def build(self, *, channel_key):
        self._dds = self.get_device(channel_key, self.DDS_TYPE)

    def init(self):
        ...

    @kernel
    def init_kernel(self):
        ...

    @kernel
    def square_pulse(self, t):
        self._dds.sw.pulse(t)

    @kernel
    def chirp_pulse(self, drg_start_ftw, drg_stop_ftw):
        # Write drg_start_ftw and drg_stop_ftw to corresponding registers of AD9910
        ...
        


class LaserModule(DaxModule):
    def build(self):
        self.raman_beam1 = DDSModule(self, 'rb1', channel_key="urukul0_ch0")
        self.raman_beam2 = DDSModule(self, 'rb2', channel_key="urukul0_ch1")

    def init(self):
        ...

    def post_init(self):
        ...

